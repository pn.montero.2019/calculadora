# In this program we will create a simple calculator
# # Define constants

N1 = 1
N2 = 2
N3 = 3
N4 = 4
N5 = 5
N6 = 6
N7 = 7
N8 = 8


# Function 'sumar' (to add)
def sumar(a, b):
    return a + b


# Function 'restar' (to subtract)
def restar(a, b):
    return a - b


# Main program
if __name__ == "__main__":
    # Use the constants defined at the beginning of the program
    # to add
    print("La suma de {} y {} = {}".format(N1, N2, sumar(N1, N2)))

    print("La suma de {} y {} = {}".format(N3, N4, sumar(N3, N4)))

    # to subtract
    print("La resta de {} y {} = {}".format(N5, N6, restar(N5, N6)))

    print("La resta de {} y {} = {}".format(N7, N8, restar(N7, N8)))
